'use strict';

// Преобразование pug -> html

module.exports = function() {

    $.gulp.task('pug', function () {
        return $.gulp.src('src/pug/page/**/*.pug')
            .pipe($.gp.plumber())                           
            .pipe($.gp.pug({
                pretty: true                                //чтобы html не был в строчку
            }))
            .pipe($.gulp.dest('dest'))
            .on('end', $.bs.reload);                        
    });
};
