'use strict';

// Обработка изображений

module.exports = function() {

    $.gulp.task('img', function (done) {
        return $.gulp.src(['src/static/img/**/*.{png,jpg,svg,gif}',
                           'src/vendor/slick/ajax-loader.gif'])    // {png,jpg,gif}
            .pipe($.gp.plumber())                                   // отлаливаю ошибки и не даю вылететь gulp
            // .pipe($.gp.imagemin())                               // сжимаю изоброжения
            .pipe($.gulp.dest('dest/img/'))
            .pipe($.bs.reload({
                stream:true                                         //позволяет после пере загрузки строници оставлять экран на том же месте(не перемещать в ночало страници)
            }));
    });
};
